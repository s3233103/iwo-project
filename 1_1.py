#!usr/bin/python3
# File name: 1_1.py
# File summary: This program builds a dictionary based upon specific input, in which the key is an ID and the value is a tuple containing the corresponding user, text and words.
# Author: Menno Robben, S3233103
# Date: 10-09-2017


import sys
import pickle


def main():
    tweetdict = {}
    postingdict = {}
    for line in sys.stdin:
        splitted = line.split("\t")
        tweetdict[splitted[0]] = ((splitted[1],splitted[2],splitted[3]))
    with open('tweetdict.pickle','wb') as f:
        pickle.dump(tweetdict, f)
    for key, value in tweetdict.items():
        for word in value[2].rstrip().split():
            if word in postingdict:
                postingdict[word].add(key)
            else:
                postingdict[word] = set()
                postingdict[word].add(key)
    with open('postingdict.pickle', 'wb') as x:
        pickle.dump(postingdict, x)

if __name__ == "__main__":
    main()

