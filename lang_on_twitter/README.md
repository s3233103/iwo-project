**### What is this repository for? ###**

This folder contains all the files needed to retrieve gender and language data from twitter.

**### What do I need? ###**

In order to properly use all scripts in this folder, you should:
  *  Have access to the /net/corpora directory of the University of Groningen
  *  Have a local copy of all the files in this folder
  *  Download langid into this folder, using the command pip install langid or pip3 install langid (see their website if this still raises errors: https://github.com/saffsd/langid.py)

**### How does it work? ###**

get-gender.py
=============
A python program by B. Plank (see https://github.com/bplank/dutchnames) that estimates the gender by username.

* In order for the script to run properly, the folder "Names" which comes with the script should be in the same folder.

## Source of names
The list of names was obtained from Wikitionary, with manual modifications:

https://en.wiktionary.org/wiki/Category:Dutch_female_given_names
https://en.wiktionary.org/wiki/Category:Dutch_male_given_names

## Example usage:

zless /net/corpora/twitter2/Tweets/2017/01/20170102\:01.out.gz  | /net/corpora/twitter2/tools/tweet2tab user | python get-gender.py


wordlists.sh
============
A shellscript that returns number of tweets, number of words in tweets and makes filtered lists for both males and females.

* The script filters the tweets of "RT"-marks, mentions and hyperlinks.
* In order to make this script run properly, the files for get-gender.py should be in the same folder as this script.
* If you have access tot the /net/corpora folder, you can run the following commands while in the folder of the script to run it:
    step 1, give it execution permission: **chmod +x wordlists.sh**
    step 2, run the program: **./wordlists.sh**
* Output file are: men-words.txt for a wordlist of male-users and women-words.txt for a wordlist of female-users.


lang_lists.sh
=============
A shellscript that takes the output files of wordlists.sh and filter the list on stopwords. After that, it uses langid to estimate the language per word and writes all results to a file.

* The list "stopwords.txt" should be in the same folder to make the script run
* The files men-words.txt and women-words.txt should be created and in the same folder as they are used as the input files.
* Langid should be installed in the folder where this script is ran.


langcounter.py
==============
A python3 script that takes 2 commandline arguments and returns a formatted output in which ISO codes for English, Dutch and other languages are counted.

* The input files should be line by line lists of tuples, in which the first tuple index is an ISO-abrreviaton.
* To run the script you should:
    step 1, make it executable: **chmod +x langcounter.py**
    step 2, add the commandline arguments in the order of file-with-mens-data file-with-women-data. As you stick to the names provided by the programs in this folder, an example call of the program will look like this:
    **./langcounter.py menlang.txt womenlang.txt**

**### Who do I talk to? ###**

- Questions and comments can be sent to *m.robben@student.rug.nl*
