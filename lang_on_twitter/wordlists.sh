#!/bin/bash
echo
echo "Number of male-tweets in the data-set:" 
zless /net/corpora/twitter2/Tweets/2017/01/20170102:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -iw "^male" | wc -l
echo
echo "Making filtered wordlist 'men-words.txt'..."
zless /net/corpora/twitter2/Tweets/2017/01/20170102:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -iw "^male" | cut -f3 | sed 's/RT//ig' | sed 's/@[^ ]* //g' | sed -e 's|http[s]\?://\S*||g' | sed 's/[^ [:alpha:]]//g' | egrep -io '\w+' | tr '[:upper:]' '[:lower:]' > men-words.txt
echo "Done."
echo
echo "Total amount of words in tweets from men:"
cat men-words.txt | wc -l
echo
echo "Number of female-tweets in the data-set:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -iw "^female" | wc -l
echo
echo "Making filtered wordlist 'women-words.txt'..."
zless /net/corpora/twitter2/Tweets/2017/01/20170102:*.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -iw "^female" | cut -f3 | sed 's/RT//ig' | sed 's/@[^ ]* //g' | sed -e 's|http[s]\?://\S*||g' | sed 's/[^ [:alpha:]]//g' | egrep -io '\w+' | tr '[:upper:]' '[:lower:]' > women-words.txt
echo "Done."
echo
echo "Total amount of words in tweets from women:"
cat women-words.txt | wc -l
echo
