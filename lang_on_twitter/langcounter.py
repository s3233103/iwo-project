#!/usr/bin/python3

import sys

def lang_counter(fname, gender_arg):
    """This function counts ISO-abbreviations for English, Dutch and other languages. Input file should be a list of tuples where the first index is the ISO-code"""
    gender = gender_arg
    english = 0
    dutch = 0
    other = 0
    for line in fname:
        if line[2:4] == "en":
            english += 1
        elif line[2:4] == "nl":
            dutch +=  1
        else:
            other += 1
    print("Numbers calculated for {0}:".format(gender))
    print("Number of English words:", english)
    print("Number of Dutch words:", dutch)
    print("Number of words in any other language:", other)

def main(argv):
    if len(argv) > 2:
        men_file = open(sys.argv[1], 'r')
        women_file = open(sys.argv[2], 'r')
        lang_counter(men_file, "men")
        print("\n")
        lang_counter(women_file, "women")
    else:
        print("!!! Usage error: {} is a progam, where the 2 arguments should be files with langid-produced tuples. First file for men, then for women.".format(argv[0]), file=sys.stderr)
        exit(-1)

if __name__ == "__main__":
    main(sys.argv)
