!/bin/bash
echo
echo "Determining language per word for men (output in 'menlang.txt')..."
while read p; do 
    if ! [[ "$p" =~ $(echo ^\($(paste -sd'|' stopwoorden.txt)\)$) ]]; then
        echo $p | langid >> menlang.txt
    fi
done < men-words.txt
echo "Done."
echo
echo "Determining language per word for women (output in 'womenlang.txt')..."
while read p; do 
    if ! [[ "$p" =~ $(echo ^\($(paste -sd'|' stopwoorden.txt)\)$) ]]; then
       echo $p | langid >> womenlang.txt
    fi
done < women-words.txt
echo "Done."  
echo
