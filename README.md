**### What is this repository for? ###**

This repository is created for the IWO-Project of Menno Robben, in which project-related files will be commited.

**### Where can I find what? ###**
### Folder week5 ###
  *  README.md-file with instructions
  *  sample-tweets.sh

### Folder lang_on_twitter ###
  * README.md-file with folder-local instructions.
  * wordlists.sh, a filtered wordlist creator for people with access to in-house twitter-data of the University of Groningen.
  * lang_lists.sh, a list creator that creates tuple-pairs with estimated languages of input-strings.
  * langcounter.py, a python3 programm used to count ISO country codes for files with layouts similar to the output of lang_lists.sh
  * get-gender.py, a python-script for estimating gender out of usernames
  * names, a folder containing the namelists for get-gender.py
  * stopwords.txt, a file of Dutch stopwords which can be used to clear data

**### Who do I talk to? ###**

- Questions and comments can be sent to *m.robben@student.rug.nl*
