#!usr/bin/python3
# File name: 1_4.py
# File summary: This program reads words from standard input and returns all tweets (in a specific set) that contain that word.
# Author: Menno Robben, S3233103
# Date: 10-09-2017

import sys
import pickle

def main():
    tweet_dict = pickle.load(open('tweetdict.pickle', 'rb'))
    posting_dict = pickle.load(open('postingdict.pickle', 'rb'))
    for line in sys.stdin:
        if len(line.split()) == 1:
            if line.rstrip() in posting_dict:
                print('"{}" occurs in:'.format(line.rstrip()))
                for tweetID in list(posting_dict[line.rstrip()]):
                    if tweetID in tweet_dict:
                        print(tweet_dict[tweetID][1] + "\n")
        else:
            first_word, second_word = line.split()
            if first_word in posting_dict and second_word in posting_dict:
                set1 = posting_dict[first_word]
                set2 = posting_dict[second_word]
                shared = set1.intersection(set2)
                if len(shared) != 0:
                    print('{0} and {1} occur in:'.format(line.split()[0], line.split()[1]))
                    for ids in shared:
                        print(tweet_dict[ids][1] + "\n")
                else:
                    print('{0} and {1} do not occur in the same tweet.'.format(line.split()[0], line.split()[1]))

if __name__ == "__main__":
    main()
