The execute the shellscript 'sample-tweets.sh' which can be found in this folder:

1. Use an LWP from the RUG or log in to Karora in order to be able to access the twitter data.
2. Make sure that you are in your own home-directory.
3. Run the script with the following commandline:
   ./sample-tweets.sh
   or if you save the file somewhere else than in your home directory:
   ./*your path*/sample-tweets.sh
