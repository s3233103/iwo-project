#!/bin/bash
echo "Number of tweets in the sample file:"
#read the content of the .out.gz-file | display the usernames of every single tweet in the file on a new line | count the total lines of previous command-ouput
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i user | wc -l
echo "Number of unique tweets in the sample file:"
#read the content of the .out.gz-file | display the texts of every single tweet in the file | sorts all tweets while removing duplicates | count the total number of lines of previous command-output
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort -u | wc -l
echo "Number of unique retweets in the sample file:"
#read the content of the .out.gz-file | display the texts of every single tweet in the file | sort all tweets while removing duplicates | search for tweets starting with "RT' | count total number of matches from previous command-output
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort -u | grep -i '^rt' | wc -l
echo "First 20 unique non-retweet-tweets in the sample file:"
#read the content of the .out.gz-file | display the texts of every single tweet in the file | search for tweets that DON'T start with RT | filter out duplicates without changing the order of the previous output | display the first 20 unique non-retweet-tweets
echo #extra whitespace to make the output a little easier to read
zless /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | grep -iv '^rt' | awk '!x[$0]++' | head -20
echo #extra whitespace to make the output a little easier to read 
